function countGate(id){
	this.type = "countGate";
	this.id = id;
	this.count = 0;
	this.cPinreset;
	this.cPincount;
	this.cPinon;
	this.cPout1;
	this.cPout2;
	this.cPout4;
	this.cPout8;
	this.countText;
	
	this.draw = function(x, y, scale){
		if(typeof(scale) === 'undefined') scale = 10;
		var group = new Kinetic.Group({
			x: x,
			y: y,
			draggable: true
		});
		
		var base = new Kinetic.Rect({
	        x: x,
	        y: y,
	        width: 25*scale,
	        height: 15*scale,
	        fill: 'yellow',
	        stroke: 'black',
	        strokeWidth: 2
	    });
		
		this.countText = new Kinetic.Text({
			x: x+(scale*15),
			y: y+(scale*9),
			width: 8*scale,
			height: 6*scale,
			text: this.count,
			fontFamily: 'Calibri',
	        fontSize: 5*scale,
	        align: 'center',
	        fill: 'black',
	    });
		
		var middlebox = new Kinetic.Rect({
			x: x+(scale*15),
			y: y+(scale*8),
			width: 8*scale,
			height: 6*scale,
			strokeWidth: 2,
			strokeColor: 'black'
		});
		
		var count = new Kinetic.Text({
			x: x+(scale*4),
			y: y+(scale*3),
			text: "tel puls.",
			fontFamily: 'Calibri',
			fontSize: 2*scale,
			fill: 'black',
		});	
		
		var on = new Kinetic.Text({
			x: x+(scale*4),
			y: y+(scale*7),
			text: "aan/uit",
			fontFamily: 'Calibri',
	        fontSize: 2*scale,
	        fill: 'black',
		});	

		var reset = new Kinetic.Text({
			x: x+(scale*4),
			y: y+(scale*11),
			text: "reset",
			fontFamily: 'Calibri',
			fontSize: 2*scale,
			fill: 'black',
		});	
		
		var text1 = new Kinetic.Text({
			x: x+(scale*22),
			y: y+(scale*4),
			text: "1",
			fontFamily: 'Calibri',
			fontSize: 2*scale,
			width: 2*scale,
			align: 'center',
			fill: 'black',
		});	
		
		var text2 = new Kinetic.Text({
			x: x+(scale*19),
			y: y+(scale*4),
			text: "2",
			fontFamily: 'Calibri',
			fontSize: 2*scale,
			width: 2*scale,
			align: 'center',
			fill: 'black',
		});	
		
		var text4 = new Kinetic.Text({
			x: x+(scale*16),
			y: y+(scale*4),
			text: "4",
			fontFamily: 'Calibri',
			fontSize: 2*scale,
			width: 2*scale,
			align: 'center',
			fill: 'black',
		});	
		
		var text8 = new Kinetic.Text({
			x: x+(scale*13),
			y: y+(scale*4),
			text: "8",
			fontFamily: 'Calibri',
			fontSize: 2*scale,
			width: 2*scale,
			align: 'center',
			fill: 'black',
		});	
		
		this.cPincount = collectionObject.addConnectionPoint(x+(2*scale), y+(4*scale), false, this, true);
		this.cPinon = collectionObject.addConnectionPoint(x+(2*scale), y+(8*scale), false, this, true);
		this.cPinreset = collectionObject.addConnectionPoint(x+(2*scale), y+(12*scale), false, this, true);
	    
		this.cPout1 = collectionObject.addConnectionPoint(x+(23*scale), y+(2*scale), false, this, false);
		this.cPout2 = collectionObject.addConnectionPoint(x+(20*scale), y+(2*scale), false, this, false);
		this.cPout4 = collectionObject.addConnectionPoint(x+(17*scale), y+(2*scale), false, this, false);
		this.cPout8 = collectionObject.addConnectionPoint(x+(14*scale), y+(2*scale), false, this, false);
	    
	    group.add(base);
	    group.add(this.countText);
	    group.add(middlebox);

	    group.add(count);
	    group.add(on);
	    group.add(reset);
	    
	    group.add(text1);
	    group.add(text2);
	    group.add(text4);
	    group.add(text8);
	    
	    group.add(this.cPinreset.draw(scale));
	    group.add(this.cPinon.draw(scale));
	    group.add(this.cPincount.draw(scale));

	    group.add(this.cPout8.draw(scale));
	    group.add(this.cPout4.draw(scale));
	    group.add(this.cPout2.draw(scale));
	    group.add(this.cPout1.draw(scale));
	    
	    var context = this;
	    
	    group.on('click', function(){
	    	if(collectionObject.pointerMode == 'pointer'){
	    		if(collectionObject.pointerMode = 'pointer'){
		    		collectionObject.resetForm();
		    	}
	    	}
	    	if(collectionObject.pointerMode == 'delete'){
	    		collectionObject.deleteGate(context, new Array(context.cPincount, context.cPinon, context.cPinreset, 
	    									context.cPout1, context.cPout2, context.cPout4, context.cPout8), group);
	    	}
	    });
	    
	    group.on('dragend dragmove', function(){
	    	context.cPincount.updateWirePositions();
	    	context.cPinon.updateWirePositions(); 
	    	context.cPinreset.updateWirePositions(); 
			
	    	context.cPout1.updateWirePositions(); 
	    	context.cPout2.updateWirePositions(); 
	    	context.cPout4.updateWirePositions(); 
	    	context.cPout8.updateWirePositions();
	    });
	    return group;
	};
	
	this.notifyVoltageChanged = function(connectionPoint){
		if(connectionPoint == this.cPinon || connectionPoint ==  this.cPinreset || connectionPoint == this.cPincount){
			if(this.cPinon.voltage == 5.0){
				this.cPinon.setState(true);
				
				if(connectionPoint == this.cPincount && this.cPincount.voltage == 5.0){
					var context = this;
					this.count++;
					if(this.count > 15){
						this.count = 0;
					}
					this.cPincount.setState(true);
					setTimeout(function(){ context.cPincount.setState(false); }, 100);
				}
			}else{
				this.cPinon.setState(false);
			}
			
			if(this.cPinreset.voltage == 5.0){
				this.count = 0;
				this.cPinreset.setState(true);
			}else{
				this.cPinreset.setState(false);
			}
			
			this.countText.setText(this.count);
			var binaryarray = this.count.toString(2).split("").reverse();
			
			if(binaryarray[0] == "1"){
				this.cPout1.setVoltage(5.0);
				this.cPout1.setState(true);				
			}else if(this.cPout1.voltage > 0){
				this.cPout1.setVoltage(0.0);
				this.cPout1.setState(false);
			}
			if(binaryarray[1] == "1"){
				this.cPout2.setVoltage(5.0);
				this.cPout2.setState(true);				
			}else if(this.cPout2.voltage > 0){
				this.cPout2.setVoltage(0.0);
				this.cPout2.setState(false);
			}
			if(binaryarray[2] == "1"){
				this.cPout4.setVoltage(5.0);
				this.cPout4.setState(true);				
			}else if(this.cPout4.voltage > 0){
				this.cPout4.setState(false);
				this.cPout4.setVoltage(0.0);
			}
			
			if(binaryarray[3] == "1"){
				this.cPout8.setVoltage(5.0);
				this.cPout8.setState(true);				
			}else if(this.cPout8.voltage > 0){
				this.cPout8.setVoltage(0.0);
				this.cPout8.setState(false);
			}
		}
		return;
	};
};
