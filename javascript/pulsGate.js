function pulsGate(id){
	this.type = "pulsGate";
	this.id = id;
	this.freq = 0;
	this.interval = 0;
	this.cPout;
	
	this.draw = function(x, y, scale){
		if(typeof(scale) === 'undefined') scale = 10;
		var group = new Kinetic.Group({
			x: x,
			y: y,
			draggable: true
		});
		
		var base = new Kinetic.Rect({
	        x: x,
	        y: y,
	        width: 20*scale,
	        height: 10*scale,
	        fill: 'yellow',
	        stroke: 'black',
	        strokeWidth: 2
	    });
	    
	    this.cPout = collectionObject.addConnectionPoint(x+(18*scale), y+(5*scale), false, this, false);
	    
	    var freqText = new Kinetic.Text({
			x: x+(scale*2),
			y: y+(scale*3),
			width: 14*scale,
			height: 6*scale,
			text: this.freq+"Hz",
			fontFamily: 'Calibri',
	        fontSize: 4*scale,
	        align: 'center',
	        fill: 'black',
	    });
		
		var middlebox = new Kinetic.Rect({
			x: x+(scale*2),
			y: y+(scale*2),
			width: 14*scale,
			height: 6*scale,
			strokeWidth: 2,
			strokeColor: 'black'
		});
		
		group.add(base);
		group.add(freqText);
	    group.add(middlebox);
	    group.add(this.cPout.draw(scale));
	    
	    var context = this;
	    
	    group.on('click', function(){
	    	if(collectionObject.pointerMode == 'pointer'){
		    	$("#form").show();
				$("#value").attr("max", "10");
		    	$("#value").val(context.freq);
		    	$("#form").submit(function(){
		    		if($.isNumeric($("#value").val())){
		    			if($("#value").val() >= 0 &&  $("#value").val() <=  10){
							context.freq = $("#value").val();
							if(context.interval > 0) clearInterval(context.interval);
							context.interval = setInterval(function(){
								context.cPout.setVoltage(5);
								context.cPout.setState(true);
								console.log("enabled");
								setTimeout(function(){
									context.cPout.setVoltage(0);
									context.cPout.setState(false);
								},100);
							}, (1/context.freq)*1000);
							freqText.setText(context.freq+"Hz");
							collectionObject.gateLayer.draw();
							
		    			}
		    		}
		    		
		    		return false;
		    	});
	    	}
	    	if(collectionObject.pointerMode == 'delete'){
	    		collectionObject.deleteGate(context, new Array(context.cPout), group);
	    	}
	    });
	    
	    group.on('dragend dragmove', function(){
	    	context.cPout.updateWirePositions();
	    });
	    return group;
	};
	
	this.notifyVoltageChanged = function(connectionPoint){
		return;
	};
};
