function compGate(id){
	this.type = "compGate";
	this.id = id;
	this.on = false;
	this.cPin;
	this.cPout;
	this.refVoltage = 2.5;
	
	this.draw = function(x, y, scale){
		if(typeof(scale) === 'undefined') scale = 10;
		var group = new Kinetic.Group({
			x: x,
			y: y,
			draggable: true
		});
		
		var base = new Kinetic.Rect({
	        x: x,
	        y: y,
	        width: 20*scale,
	        height: 10*scale,
	        fill: 'yellow',
	        stroke: 'black',
	        strokeWidth: 2
	    });
	    
		this.cPin = collectionObject.addConnectionPoint(x+(2*scale), y+(2*scale), false, this, true);
	    this.cPout = collectionObject.addConnectionPoint(x+(18*scale), y+(5*scale), false, this, false);
	    
	    var triangle = new Kinetic.RegularPolygon({
	    	x: x+(10*scale),
	        y: y+(5*scale),
	        sides: 3,
	        radius: 4*scale,
	        stroke: 'black',
	        strokeWidth: 2,
	        rotation: (Math.PI/2)
	    });
	    
	    var plus =  new Kinetic.Text({
	    	text: '+',
	    	fontFamily: 'Calibri',
	    	fontSize: 16,
	    	x: x+(8*scale),
	    	y: y+(2*scale),
	    	fill: 'black'
	    });
	    
	    var min =  new Kinetic.Text({
	    	text: '-',
			fontFamily: 'Calibri',
	        fontSize: 20,
	        x: x+(8.1*scale),
	        y: y+(6*scale),
			fill: 'black'
	    });
	    
	    var voltageText = new Kinetic.Text({
			x: x+(scale*1),
			y: y+(scale*6.5),
			width: 6*scale,
			height: 6*scale,
			text: "+"+this.refVoltage+"V",
			fontFamily: 'Calibri',
	        fontSize: 2*scale,
	        align: 'center',
	        fill: 'black',
	    });
	    
	    group.add(base);
	    group.add(triangle);
	    group.add(plus);
	    group.add(min);
	    group.add(voltageText);
	    group.add(this.cPin.draw(scale));
	    group.add(this.cPout.draw(scale));
	    
	    var context = this;
	    
	    group.on('click', function(){
	    	if(collectionObject.pointerMode == 'pointer'){
		    	$("#form").show();
		    	$("#value").val(context.refVoltage);
		    	$("#form").submit(function(){
		    		if($.isNumeric($("#value").val())){
		    			if($("#value").val() >= 0 &&  $("#value").val() <=  5){
		    				context.refVoltage = $("#value").val();
		    			}
		    		}		    		
		    	});
	    	}
	    	if(collectionObject.pointerMode == 'delete'){
	    		collectionObject.deleteGate(context, new Array(context.cPout, context.cPin), group);
	    	}
	    });
	    
	    group.on('dragend dragmove', function(){
	    	context.cPout.updateWirePositions();
	    });
	    return group;
	};
	
	this.notifyVoltageChanged = function(connectionPoint){
		if(connectionPoint ==  this.cPin){
			if(this.cPin.voltage >= this.refVoltage){
				this.cPin.setState(true);
				this.cPout.setState(true);
				this.cPout.setVoltage(5.0);				
			}else{
				this.cPin.setState(false);
				this.cPout.setState(false);
				this.cPout.setVoltage(0.0);
			}
		}
	};
};
