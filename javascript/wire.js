function wire(wireId, startpoint, endpoint){
	this.id = wireId;
	this.startPoint = startpoint;
	this.endPoint = endpoint;
	this.voltage = 0;
	this.line;
	
	this.setVoltage = function(voltage){
		console.log("wire.setvoltage called with value: "+voltage);
		this.voltage = voltage; 
		//stroom gaat alleen van output naar input
		//this.startPoint.setVoltage(voltage);
		this.endPoint.setVoltage(voltage);
	};
	
	/*this.setVoltageFromPoint = function(voltage, cpoint){
		this.voltage = voltage;
		if(this.startPoint != cpoint){
			this.startPoint.setVoltage(voltage);
		}
		if(this.endPoint != cpoint){
			this.endPoint.setVoltage(voltage);
		}
	};*/
	
	this.draw = function(){
		this.line = new Kinetic.Line({
	        points: [this.startPoint.getXY().x, this.startPoint.getXY().y, this.endPoint.getXY().x, this.endPoint.getXY().y,],
	        stroke: 'black',
	        strokeWidth: 5,
	        fill: 'black',
	        lineCap: 'round',
	        lineJoin: 'round'
	    });
		
		var context = this;
		this.line.on('click', function(){
			if(collectionObject.pointerMode == 'delete'){
				collectionObject.deleteWire(context);
			}
		});
		console.log("typeof line: "+this.line);
		return this.line;
	};
	
	this.updatePosition = function(){
		this.line.setPoints([this.startPoint.getXY().x, this.startPoint.getXY().y, this.endPoint.getXY().x, this.endPoint.getXY().y,]);
		collectionObject.wireLayer.draw();
	};
	
};