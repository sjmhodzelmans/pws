function andGate(id){
	this.type = "andGate";
	this.id = id;
	this.on = false;
	this.cPintop;
	this.cPinbottom;
	this.cPout;
	
	this.draw = function(x, y, scale){
		if(typeof(scale) === 'undefined') scale = 10;
		var group = new Kinetic.Group({
			x: x,
			y: y,
			draggable: true
		});
		
		var base = new Kinetic.Rect({
	        x: x,
	        y: y,
	        width: 20*scale,
	        height: 10*scale,
	        fill: 'yellow',
	        stroke: 'black',
	        strokeWidth: 2
	    });
		
		var andSign = new Kinetic.Text({
			x: x+(scale*5),
			y: y+(scale*3),
			width: 10*scale,
			height: 6*scale,
			text: "&",
			fontFamily: 'Calibri',
	        fontSize: 5*scale,
	        align: 'center',
	        fill: 'black',
	    });
		
		var middlebox = new Kinetic.Rect({
			x: x+(scale*5),
			y: y+(scale*2),
			width: 10*scale,
			height: 6*scale,
			strokeWidth: 2,
			strokeColor: 'black'
		});
	    
		this.cPintop = collectionObject.addConnectionPoint(x+(2*scale), y+(2*scale), false, this, true);
		this.cPinbottom = collectionObject.addConnectionPoint(x+(2*scale), y+(8*scale), false, this, true);
	    this.cPout = collectionObject.addConnectionPoint(x+(18*scale), y+(5*scale), false, this, false);
	    
	    group.add(base);
	    group.add(andSign);
	    group.add(middlebox);
	    
	    group.add(this.cPintop.draw(scale));
	    group.add(this.cPinbottom.draw(scale));
	    group.add(this.cPout.draw(scale));
	    
	    var context = this;
	    
	    group.on('click', function(){
	    	if(collectionObject.pointerMode == 'pointer'){
	    		if(collectionObject.pointerMode = 'pointer'){
		    		collectionObject.resetForm();
		    	}
	    	}
	    	if(collectionObject.pointerMode == 'delete'){
	    		collectionObject.deleteGate(context, new Array(context.cPintop, context.cPinbottom, context.cPout), group);
	    	}
	    });
	    
	    group.on('dragend dragmove', function(){
	    	context.cPout.updateWirePositions();
	    	context.cPinbottom.updateWirePositions();
	    	context.cPintop.updateWirePositions();
	    });
	    return group;
	};
	
	this.notifyVoltageChanged = function(connectionPoint){
		if(connectionPoint == this.cPinbottom || connectionPoint == this.cPintop){
			if(this.cPintop.voltage == 5.0){
				this.cPintop.setState(true);
			}else{
				this.cPintop.setState(false);
			}
			if(this.cPinbottom.voltage == 5.0){
				this.cPinbottom.setState(true);
			}else{
				this.cPinbottom.setState(false);
			}
			
			if(this.cPintop.voltage == 5.0 && this.cPinbottom.voltage == 5.0){
				this.cPout.setVoltage(5.0);
				this.cPout.setState(true);
			}else{
				this.cPout.setVoltage(0.0);
				this.cPout.setState(false);
			}
		}
		return;
	};
};
