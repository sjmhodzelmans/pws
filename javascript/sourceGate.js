function sourceGate(id){
	this.type = "sourceGate";
	this.id = id;
	this.on = false;
	this.cPout;
	
	this.draw = function(x, y, scale){
		if(typeof(scale) === 'undefined') scale = 10;
		group = new Kinetic.Group({
			x: x,
			y: y,
			draggable: true
		});
		
		var base = new Kinetic.Rect({
	        x: x,
	        y: y,
	        width: 20*scale,
	        height: 10*scale,
	        fill: 'yellow',
	        stroke: 'black',
	        strokeWidth: 2
	    });
	    /*
	    var out1 = new Kinetic.Circle({
	    	x: x+(18*scale),
	    	y: y+(5*scale),
	    	radius: 1*scale,
	    	fill: 'black',
	    	stroke: 'blue',
	    	stroleWidth: 2,
	    	name: 'out1'
	    });*/
	    
	    this.cPout = collectionObject.addConnectionPoint(x+(18*scale), y+(5*scale), false, this, false);
	    
	    group.add(base);
	    group.add(this.cPout.draw(scale));
	    
	    var context = this;
	    
	    group.on('click', function(){
	    	collectionObject.resetForm();
	    	console.log('pointerMode: '+collectionObject.pointerMode);
	    	if(collectionObject.pointerMode == 'pointer' && context.on == false){
		    	context.cPout.setState(true);
		    	context.cPout.setVoltage(5.0);
				console.log("click function fired, context.on was false");
				setTimeout(function(){
					if(context.on == false){
						context.cPout.setState(false);
						context.cPout.setVoltage(0.0);
					}
				}, 1000);
	    	}
	    	if(collectionObject.pointerMode == 'delete'){
	    		collectionObject.deleteGate(context, new Array(context.cPout), group);
	    		//group.removeChildren();
	    		//collectionObject.gateLayer.remove(group);
	    		//collectionObject.gateLayer.draw();
	    	}
	    });
		
		group.on('dblclick', function(){
			if(collectionObject.pointerMode == 'pointer'){
				console.log("context.on: "+context.on);
				if(context.on == true){
					context.on = false;
					context.cPout.setState(false);
					context.cPout.setVoltage(0.0);
				}else{
					context.on = true;
					context.cPout.setState(true);
					context.cPout.setVoltage(5.0);
				}
			}
			console.log("dblclick fired");
			console.log("context.on: "+context.on);
		});
	    
	    group.on('dragend dragmove', function(){
	    	context.cPout.updateWirePositions();
	    });
	    return group;
	};
	
	this.notifyVoltageChanged = function(connectionPoint){
		console.log("sourceGate.notifyVoltageChanged called");
		return;
	};
};
