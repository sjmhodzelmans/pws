function ledGate(id){
	this.type = "ledGate";
	this.id = id;
	this.on = false;
	this.connectionPointInput;
	this.led;
	
	this.draw = function(x, y, scale){
		if(typeof(scale) === 'undefined') scale = 10;
		
		var group = new Kinetic.Group({
			x: x,
			y: y,
			draggable: true
		});
		
		var base = new Kinetic.Rect({
	        x: x,
	        y: y,
	        width: 20*scale,
	        height: 10*scale,
	        fill: 'yellow',
	        stroke: 'black',
	        strokeWidth: 2
	    });
		
		var fillcolor = 'black';
		if(this.on){
			fillcolor = 'red';
		}
	    
	    this.connectionPointInput = collectionObject.addConnectionPoint(x+(2*scale), y+(5*scale), false, this, true);
	    	
	    this.led = new Kinetic.Ellipse({
	    	x: x+(15*scale),
	    	y: y+(5*scale),
	    	radius: {
	    		x: 20,
	    		y: 10
	    	},
	    	fill: fillcolor,
	    	stroke: 'white',
	        strokeWidth: 2
	    });

	    group.add(base);
	    group.add(this.connectionPointInput.draw(scale));
	    group.add(this.led);
	    
	    var context = this;
	    
	    group.on('dragend dragmove', function(){
	    	context.connectionPointInput.updateWirePositions();
	    });
	    group.on('click', function(){
	    	if(collectionObject.pointerMode == 'pointer'){
	    		collectionObject.resetForm();
	    	}
	    	if(collectionObject.pointerMode == 'delete'){
	    		collectionObject.deleteGate(context, new Array(context.connectionPointInput), group);
	    	}
	    });
	    return group;
	};
	
	this.notifyVoltageChanged = function(connectionPoint) {
		console.log("ledGate.notifyVoltageChanged called");
		if(this.connectionPointInput == connectionPoint){
			if(connectionPoint.voltage == 5.0){
				this.led.setFill('red');
				this.connectionPointInput.setState(true);
				this.on = true;
			}else{
				this.led.setFill('black');
				this.connectionPointInput.setState(false);
				this.on = false;
			}
		}
	};
	/*
	this.clickFunction = function(){
		if(this.on){
    		this.on = false;
    		this.connectionPointInput.setState(false);
    		this.led.setFill('black');
    	}else{
    		this.on = true;
    		this.connectionPointInput.setState(true);
    		this.led.setFill('red');
    	}
	};
	*/
};


