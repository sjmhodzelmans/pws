/*reference implementation
function baseGate(id){
	this.type = "baseGate";
	this.id = id;
	//this.on = false;
	this.connectionPoints = new Array();
	
	this.getGroup = function(x, y, scale){
		if(typeof(scale) === 'undefined') scale = 10;
		
		var group = new Kinetic.Group({
			x: x,
			y: y,
			draggable: true
		});
		
		var base = new Kinetic.Rect({
	        x: x,
	        y: y,
	        width: 20*scale,
	        height: 10*scale,
	        fill: 'yellow',
	        stroke: 'black',
	        strokeWidth: 2
	    });
		
		group.add(base);
		
		return group;
	};
	
	this.notifyVoltageChanged = function(connectionPoint) {
		console.log("getting voltage update from connectionpoint: "+connectionPoint.id);
	};
}

function extend(subclass, superclass) {
	function Dummy() {}
	Dummy.prototype = superclass.prototype;
	subclass.prototype = new Dummy();
	subclass.prototype.constructor = subclass;
	subclass.superclass = superclass;
	subclass.superproto = superclass.prototype;
}*/