function connectionPoint(id, x, y, state, congate, isInput){
	this.id = id;
	this.x = x;
	this.y = y;
	this.drawing;
	this.state = state;
	/**
	 * @var gate the gate that it is connected to
	 */
	this.gate = congate;
	this.voltage = 0.0;
	/**
	 * @var the connected wires
	 */
	this.wires = new Array();
	this.isInput = isInput;
	
	this.draw = function(scale){
		var fillColor = 'black';
		if(this.state){
			fillColor = 'red';
		}
		this.drawing = new Kinetic.Circle({
	    	x: this.x,
	    	y: this.y,
	    	radius: scale,
	    	fill: fillColor,
	    	stroke: 'blue',
	    	stroleWidth: 2,
	    	name: 'out1'
	    });
		
		var id = this.id;
		/*console.log("testPoint.voltage: "+testPoint.voltage);
		console.log("this.voltage: "+this.voltage);*/
		
		console.log('this.isInput: '+this.isInput);
		this.drawing.on('click', function(){
			if(collectionObject.pointerMode == 'wire'){
				var thisPoint = collectionObject.getPointById(id);
				console.log("thisPoint.voltage: "+thisPoint.voltage);
				if(thisPoint.isInput == false && collectionObject.startPointNewWire == null){
					collectionObject.startPointNewWire = thisPoint;
//					console.log('it was an input and the first to be clicked');
//					console.log('startPointNewWire.id: '+context.id);
//					console.log('startPointNewWire.voltage: '+context.voltage);
				}else if(thisPoint.isInput == true && collectionObject.startPointNewWire != null){
					collectionObject.addWire(collectionObject.startPointNewWire, thisPoint);
					collectionObject.startPointNewWire = null;
				}
			}
		});
		
		
		return this.drawing;
	};
	
	this.setState = function(newState){
		if(newState){
			this.drawing.setFill('red');
			this.state = true;
		}else{
			this.drawing.setFill('black');
			this.state = false;
		}
		collectionObject.gateLayer.draw();
		return this.state;
	};
	
	
	this.getXY = function(cPoint){
		this.x = this.drawing.getAbsolutePosition().x;
		this.y = this.drawing.getAbsolutePosition().y;
		//console.log("this.x: "+this.x);
		//console.log("this.y: "+this.y);
		var returnobject = {};
		returnobject.x = this.x;
		returnobject.y = this.y;
		return returnobject;
	};
	
	this.setVoltage = function(voltage) {
		console.log("connectionPoint.id: "+this.id);
		console.log("connectionPoint.setVoltage called with voltage: "+voltage);
		this.voltage = voltage;
		if(this.wires.length >= 1 && this.isInput == false){
			for(var key in this.wires){
				this.wires[key].setVoltage(voltage);
			}
		}else if(this.wires.length >= 1 && this.isInput == true){
			this.voltage = 0;
			for(var key in this.wires){
				var wire = this.wires[key];
				if(wire.voltage > this.voltage){
					this.voltage = wire.voltage;
				}
			}
		}
		this.gate.notifyVoltageChanged(this);
	};
	
	this.addWire = function(wire){
		console.log("typeof wire is: "+typeof(wire));
		this.wires.push(wire);
	};
	
	this.removeWire = function(wire){
		console.log("typeof wire is: "+typeof(wire));
		this.wires.splice(this.wires.indexOf(wire), 1);
	};
	
	this.updateWirePositions = function(){
		console.log("updateWirePositions fired");
		//console.log("typeof this.wires[0]: "+typeof(this.wires[0]));
		for(var key in this.wires){
			var w = this.wires[key];
			console.log("typeof inWires: "+typeof(w));
			w.updatePosition();
		}
	};
};

