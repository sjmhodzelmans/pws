function buzzGate(id){
	this.type = "buzzGate";
	this.id = id;
	this.on = false;
	this.connectionPointInput;
	this.buzz;
	this.audioElement = document.getElementById("buzz");
	
	this.draw = function(x, y, scale){
		if(typeof(scale) === 'undefined') scale = 10;
		
		var group = new Kinetic.Group({
			x: x,
			y: y,
			draggable: true
		});
		
		var base = new Kinetic.Rect({
	        x: x,
	        y: y,
	        width: 20*scale,
	        height: 10*scale,
	        fill: 'yellow',
	        stroke: 'black',
	        strokeWidth: 2
	    });
		
		var strokecolor = 'black';
		if(this.on){
			strokecolor = 'red';
		}
	    
	    this.connectionPointInput = collectionObject.addConnectionPoint(x+(2*scale), y+(5*scale), false, this, true);
	    	
	    this.buzz = new Kinetic.Polygon({
	    	points: [x+(10*scale),y+(4*scale),x+(12*scale),y+(4*scale),x+(14*scale),y+(2*scale),x+(14*scale),y+(8*scale),
	    	         x+(12*scale),y+(6*scale),x+(10*scale),y+(6*scale),],
	    	stroke: strokecolor,
	    	strokeWidth: 2
	    });

	    group.add(base);
	    group.add(this.connectionPointInput.draw(scale));
	    group.add(this.buzz);
	    
	    var context = this;
	    
	    group.on('dragend dragmove', function(){
	    	context.connectionPointInput.updateWirePositions();
	    });
	    group.on('click', function(){
	    	if(collectionObject.pointerMode == 'pointer'){
	    		collectionObject.resetForm();
	    	}
	    	if(collectionObject.pointerMode == 'delete'){
	    		collectionObject.deleteGate(context, new Array(context.connectionPointInput), group);
	    	}
	    });
	    return group;
	};
	
	this.notifyVoltageChanged = function(connectionPoint) {
		console.log("ledGate.notifyVoltageChanged called");
		if(this.connectionPointInput == connectionPoint){
			if(connectionPoint.voltage == 5.0){
				this.buzz.setStroke('red');
				this.audioElement.play();
				this.connectionPointInput.setState(true);
				this.on = true;
			}else{
				this.buzz.setStroke('black');
				this.audioElement.pause();
				this.connectionPointInput.setState(false);
				this.on = false;
			}
		}
	};
	/*
	this.clickFunction = function(){
		if(this.on){
    		this.on = false;
    		this.connectionPointInput.setState(false);
    		this.led.setFill('black');
    	}else{
    		this.on = true;
    		this.connectionPointInput.setState(true);
    		this.led.setFill('red');
    	}
	};
	*/
};


