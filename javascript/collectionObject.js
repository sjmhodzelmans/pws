var collectionObject = {
	gateCollection : new Array(),
	wireCollection : new Array(),
	connectPointsCollection : new Array(),
	pointerMode : 'pointer',
	startPointNewWire : null,
	wireLayer : new Kinetic.Layer(),
	gateLayer : new Kinetic.Layer(),
	stage : null,
	highestPointId: 0,
	highestGateId: 0,
	highestWireId: 0,
	
	addGate : function(gateType, x, y){
		if(typeof(x) === 'undefined') x = 70;
		if(typeof(y) === 'undefined') y = 70;
		var newGate;
		switch (gateType) {
			case "ledGate":
				newGate = new ledGate(this.gateCollection.length);
				break;
			case "sourceGate":
				newGate = new sourceGate(this.gateCollection.length);
				break;
			case "andGate":
				newGate = new andGate(this.gateCollection.length);
				break;
			case "orGate":
				newGate = new orGate(this.gateCollection.length);
				break;
			case "notGate":
				newGate = new notGate(this.gateCollection.length);
				break;
			case "varGate":
				newGate = new varGate(this.gateCollection.length);
				break;
			case "compGate":
				newGate = new compGate(this.gateCollection.length);
				break;
			case "memGate":
				newGate = new memGate(this.gateCollection.length);
				break;
			case "pulsGate":
				newGate = new pulsGate(this.gateCollection.length);
				break;
			case "countGate":
				newGate = new countGate(this.gateCollection.length);
				break;
			case "adGate":
				newGate = new adGate(this.gateCollection.length);
				break;
			case "buzzGate":
				newGate = new buzzGate(this.gateCollection.length);
				break;
			default:
				return false;
		}
		this.gateLayer.add(newGate.draw(x,y));
		this.gateLayer.draw();
		this.gateCollection.push(newGate);
		this.highestGateId++;
		return newGate;
	},
	
	addWire : function(startPoint, endPoint){
		var newwire = new wire(this.wireCollection.length, startPoint, endPoint);
		//draw it
		this.wireLayer.add(newwire.draw());
		console.log("typeof newwire: "+typeof(newwire));
		this.wireLayer.draw();
		//add and return
		console.log("startPoint.id: "+startPoint.id);
		console.log("startPoint.voltage: "+startPoint.voltage);
		newwire.setVoltage(startPoint.voltage);
		this.wireCollection.push(newwire);
		startPoint.addWire(newwire);
		endPoint.addWire(newwire);
		this.highestWireId++;
		return newwire;
	},
	
	addConnectionPoint : function(x, y, state, connectedGate, isInput){
		var newCpoint = new connectionPoint(this.connectPointsCollection.length, x, y, state, connectedGate, isInput);
		this.connectPointsCollection.push(newCpoint);
		this.highestPointId++;
		return newCpoint;
	},
	
	getGateById : function(gateId){
		if(!isNaN(gateId) && gateId <= this.highestGateId){
			for ( var int = gateId; int < this.gateCollection.length; int++) {
				if(this.gatePointsCollection[int].id == gateId){
					return this.gateCollection[int];
				}
			}
		}
	},
	
	getWireById : function(wireId){
		if(!isNaN(wireId) && wireId <= this.highestWireId){
			for ( var int = wireId; int < this.wireCollection.length; int++) {
				if(this.wirePointsCollection[int].id == wireId){
					return this.wireCollection[int];
				}
			}
		}
	},
	
	getPointById : function(pointId){
		console.log("typeof this.connectPointsCollection: "+typeof(this.connectPointsCollection));
		if(!isNaN(pointId) && pointId <= this.highestPointId){
			for ( var int = pointId; int < this.connectPointsCollection.length; int++) {
				if(this.connectPointsCollection[int].id == pointId){
					return this.connectPointsCollection[int];
				}
			}
		}
	},
	
	deleteWire : function(wire) {
		var index = this.wireCollection.indexOf(wire);
		wire.line.remove();
		this.wireLayer.draw();
		wire.startPoint.removeWire(wire);
		wire.endPoint.removeWire(wire);
		this.wireCollection.splice(index, 1);
	},
	
	deleteConnectionPoint : function(cPoint) {
		var index = this.connectPointsCollection.indexOf(cPoint);
		for(var key in cPoint.wires){
			this.deleteWire(cPoint.wires[key]);
		}
		this.connectPointsCollection.splice(index, 1);
	},
	
	deleteGate : function(gate, cPointsArray, group) {
		var index = this.gateCollection.indexOf(gate);
		for(var key in cPointsArray){
			this.deleteConnectionPoint(cPointsArray[key]);
		}
		console.log("for loop in deleteGate ended");
		group.removeChildren();
		group.getParent().remove(group);
		this.gateLayer.draw();
		this.gateCollection.splice(index, 1);
	},
	
	/**
	 * drawBasis
	 * 
	 * function to draw the basis
	 * 
	 * @param stage the stage to draw the baseLayer on
	 * 
	 * @return 
	 */
	drawBasis : function(stage){
		this.stage = stage;
		console.log("drawBasis called");
		var baseLayer = new Kinetic.Layer();
		
		//{pointermodes
			var pMode = new Kinetic.Group({
				x: 1,
				y: 1,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 50,
				height: 50,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'B',
				fontFamily: 'Calibri',
		        fontSize: 20,
				x: 0,
				y: 18,
				width: 50,
				fill: 'black',
				align: 'center'
			}));
			pMode.on('click', function(){
				collectionObject.pointerMode = 'pointer';
				collectionObject.resetForm();
				console.log("set pointermode to: "+collectionObject.pointerMode);
			});
			baseLayer.add(pMode);
			
			var delMode = new Kinetic.Group({
				x: 1,
				y: 51,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 50,
				height: 50,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'V',
				fontFamily: 'Calibri',
				fontSize: 20,
				x: 0,
				y: 18,
				width: 50,
				fill: 'black',
				align: 'center'
			}));
			delMode.on('click', function(){
				collectionObject.pointerMode = 'delete';
				collectionObject.resetForm();
				console.log("set pointermode to: "+collectionObject.pointerMode);
			});
			baseLayer.add(delMode);
			
			var wireMode = new Kinetic.Group({
				x: 1,
				y: 101,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 50,
				height: 50,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'D',
				fontFamily: 'Calibri',
				fontSize: 20,
				x: 0,
				y: 18,
				width: 50,
				fill: 'black',
				align: 'center'
			}));
			wireMode.on('click', function(){
				collectionObject.pointerMode = 'wire';
				collectionObject.resetForm();
				console.log("set pointermode to: "+collectionObject.pointerMode);
			});
			baseLayer.add(wireMode);
			
		//pointermode}
			
			
		//{addgates
			var sourceGate = new Kinetic.Group({
				x: 101,
				y: 1,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 70,
				height: 40,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'BRON',
				fontFamily: 'Calibri',
				fontSize: 18,
				x: 0,
				y: 14,
				width: 70,
				fill: 'black',
				align: 'center'
			}));
			sourceGate.on('click', function(){
				collectionObject.addGate("sourceGate");
			});
			baseLayer.add(sourceGate);
			
			var ledGate = new Kinetic.Group({
				x: 171,
				y: 1,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 50,
				height: 40,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'LED',
				fontFamily: 'Calibri',
		        fontSize: 18,
				x: 0,
				y: 14,
				width: 50,
				align: 'center',
				fill: 'black'
			}));
			ledGate.on('click', function(){
				collectionObject.addGate("ledGate");
			});
			baseLayer.add(ledGate);
			
			var andGate = new Kinetic.Group({
				x: 221,
				y: 1,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 50,
				height: 40,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'EN',
				fontFamily: 'Calibri',
				fontSize: 18,
				x: 0,
				y: 14,
				width: 50,
				align: 'center',
				fill: 'black'
			}));
			andGate.on('click', function(){
				collectionObject.addGate("andGate");
			});
			baseLayer.add(andGate);
			
			var orGate = new Kinetic.Group({
				x: 271,
				y: 1,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 50,
				height: 40,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'OF',
				fontFamily: 'Calibri',
				fontSize: 18,
				x: 0,
				y: 14,
				width: 50,
				align: 'center',
				fill: 'black'
			}));
			orGate.on('click', function(){
				collectionObject.addGate("orGate");
			});
			baseLayer.add(orGate);
			
			var notGate = new Kinetic.Group({
				x: 321,
				y: 1,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 60,
				height: 40,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'NIET',
				fontFamily: 'Calibri',
		        fontSize: 18,
				x: 0,
				y: 14,
				width: 60,
				align: 'center',
				fill: 'black'
			}));
			notGate.on('click', function(){
				collectionObject.addGate("notGate");
			});
			baseLayer.add(notGate);
			
			var varGate = new Kinetic.Group({
				x: 381,
				y: 1,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 90,
				height: 40,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'VAR.BR.',
				fontFamily: 'Calibri',
				fontSize: 18,
				x: 0,
				y: 14,
				width: 90,
				align: 'center',
				fill: 'black'
			}));
			varGate.on('click', function(){
				collectionObject.addGate("varGate");
			});
			baseLayer.add(varGate);
			
			var compGate = new Kinetic.Group({
				x: 471,
				y: 1,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 70,
				height: 40,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'COMP',
				fontFamily: 'Calibri',
				fontSize: 18,
				x: 0,
				y: 14,
				width: 70,
				align: 'center',
				fill: 'black'
			}));
			compGate.on('click', function(){
				collectionObject.addGate("compGate");
			});
			baseLayer.add(compGate);
			
			var memGate = new Kinetic.Group({
				x: 541,
				y: 1,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 70,
				height: 40,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'MEM',
				fontFamily: 'Calibri',
		        fontSize: 18,
				x: 0,
				y: 14,
				width: 70,
				align: 'center',
				fill: 'black'
			}));
			memGate.on('click', function(){
				collectionObject.addGate("memGate");
			});
			baseLayer.add(memGate);
			
			var pulsGate = new Kinetic.Group({
				x: 611,
				y: 1,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 70,
				height: 40,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'FREQ',
				fontFamily: 'Calibri',
				fontSize: 18,
				x: 0,
				y: 14,
				width: 70,
				align: 'center',
				fill: 'black'
			}));
			pulsGate.on('click', function(){
				collectionObject.addGate("pulsGate");
			});
			baseLayer.add(pulsGate);
			
			var countGate = new Kinetic.Group({
				x: 681,
				y: 1,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 50,
				height: 40,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'TEL',
				fontFamily: 'Calibri',
				fontSize: 18,
				x: 0,
				y: 14,
				width: 50,
				align: 'center',
				fill: 'black'
			}));
			countGate.on('click', function(){
				collectionObject.addGate("countGate");
			});
			baseLayer.add(countGate);
			
			var adGate = new Kinetic.Group({
				x: 731,
				y: 1,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 50,
				height: 40,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'ADC',
				fontFamily: 'Calibri',
				fontSize: 18,
				x: 0,
				y: 14,
				width: 50,
				align: 'center',
				fill: 'black'
			}));
			adGate.on('click', function(){
				collectionObject.addGate("adGate");
			});
			baseLayer.add(adGate);
			
			var buzzGate = new Kinetic.Group({
				x: 781,
				y: 1,
			}).add(new Kinetic.Rect({
				x: 0,
				y: 0,
				width: 60,
				height: 40,
				stroke: 'blue',
				strokeWidth: 4
			})).add(new Kinetic.Text({
				text: 'ZOEM',
				fontFamily: 'Calibri',
		        fontSize: 18,
				x: 0,
				y: 14,
				width: 60,
				align: 'center',
				fill: 'black'
			}));
			buzzGate.on('click', function(){
				collectionObject.addGate("buzzGate");
			});
			baseLayer.add(buzzGate);
		//addgates}
		
		stage.add(this.wireLayer);
		stage.add(this.gateLayer);
		stage.add(baseLayer);
		
		baseLayer.setZIndex(3);
		this.wireLayer.setZIndex(2);
		this.gateLayer.setZIndex(1);	
		return baseLayer;
	},
	
	resetForm : function(){
		$("#form").hide();
    	$("#value").val("");
    	$("#value").attr("max", "5");
    	$("#form").submit(function(){ return false; });
	},
};