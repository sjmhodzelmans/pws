function notGate(id){
	this.type = "notGate";
	this.id = id;
	this.on = false;
	this.cPin;
	this.cPout;
	
	this.draw = function(x, y, scale){
		if(typeof(scale) === 'undefined') scale = 10;
		var group = new Kinetic.Group({
			x: x,
			y: y,
			draggable: true
		});
		
		var base = new Kinetic.Rect({
	        x: x,
	        y: y,
	        width: 20*scale,
	        height: 10*scale,
	        fill: 'yellow',
	        stroke: 'black',
	        strokeWidth: 2
	    });
		
		var notSign = new Kinetic.Text({
			x: x+(scale*5),
			y: y+(scale*3),
			width: 10*scale,
			height: 6*scale,
			text: "!1",
			fontFamily: 'Calibri',
	        fontSize: 5*scale,
	        align: 'center',
	        fill: 'black',
	    });
		
		var middlebox = new Kinetic.Rect({
			x: x+(scale*5),
			y: y+(scale*2),
			width: 10*scale,
			height: 6*scale,
			strokeWidth: 2,
			strokeColor: 'black'
		});
	    
		this.cPin = collectionObject.addConnectionPoint(x+(2*scale), y+(5*scale), false, this, true);
		this.cPout = collectionObject.addConnectionPoint(x+(18*scale), y+(5*scale), false, this, false);
	    
	    group.add(base);
	    group.add(notSign);
	    group.add(middlebox);
	    
	    group.add(this.cPin.draw(scale));
	    group.add(this.cPout.draw(scale));
	    
	    var context = this;
	    
	    group.on('click', function(){
	    	if(collectionObject.pointerMode == 'pointer'){
		    	collectionObject.resetForm();
	    	}
	    	if(collectionObject.pointerMode == 'delete'){
	    		collectionObject.deleteGate(context, new Array(context.cPin, context.cPout), group);
	    	}
	    });
	    
	    group.on('dragend dragmove', function(){
	    	context.cPout.updateWirePositions();
	    	context.cPin.updateWirePositions();
	    });
	    return group;
	};
	
	this.notifyVoltageChanged = function(connectionPoint){
		if(connectionPoint == this.cPin){
			if(this.cPin.voltage >= 2.5){
				this.cPin.setState(true);
				this.cPout.setVoltage(0.0);
				this.cPout.setState(false);
			}else{
				this.cPin.setState(false);
				this.cPout.setVoltage(5.0);
				this.cPout.setState(true);
			}
			
		}
		return;
	};
};
