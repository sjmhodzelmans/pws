function varGate(id){
	this.type = "varGate";
	this.id = id;
	this.on = false;
	this.cPout;
	
	this.draw = function(x, y, scale){
		if(typeof(scale) === 'undefined') scale = 10;
		var group = new Kinetic.Group({
			x: x,
			y: y,
			draggable: true
		});
		
		var base = new Kinetic.Rect({
	        x: x,
	        y: y,
	        width: 20*scale,
	        height: 10*scale,
	        fill: 'yellow',
	        stroke: 'black',
	        strokeWidth: 2
	    });
	    
	    this.cPout = collectionObject.addConnectionPoint(x+(18*scale), y+(5*scale), false, this, false);
	    
	    var voltageText = new Kinetic.Text({
			x: x+(scale*2),
			y: y+(scale*3),
			width: 14*scale,
			height: 6*scale,
			text: "+"+this.cPout.voltage+"V",
			fontFamily: 'Calibri',
	        fontSize: 4*scale,
	        align: 'center',
	        fill: 'black',
	    });
		
		var middlebox = new Kinetic.Rect({
			x: x+(scale*2),
			y: y+(scale*2),
			width: 14*scale,
			height: 6*scale,
			strokeWidth: 2,
			strokeColor: 'black'
		});
		
		group.add(base);
		group.add(voltageText);
	    group.add(middlebox);
	    group.add(this.cPout.draw(scale));
	    
	    var context = this;
	    
	    group.on('click', function(){
	    	if(collectionObject.pointerMode == 'pointer'){
		    	$("#form").show();
		    	$("#value").val(context.cPout.voltage);
		    	$("#form").submit(function(){
		    		if($.isNumeric($("#value").val())){
		    			if($("#value").val() >= 0 &&  $("#value").val() <=  5){
		    				context.cPout.setVoltage($("#value").val());
		    				voltageText.setText("+"+context.cPout.voltage+"V");
		    				collectionObject.gateLayer.draw();
		    			}
		    		}
		    		if(context.cPout.voltage >= 2.5){
		    			context.cPout.setState(true);
		    		}else{
		    			context.cPout.setState(false);
		    		}
		    		return false;
		    	});
	    	}
	    	if(collectionObject.pointerMode == 'delete'){
	    		collectionObject.deleteGate(context, new Array(context.cPout), group);
	    	}
	    });
	    
	    group.on('dragend dragmove', function(){
	    	context.cPout.updateWirePositions();
	    });
	    return group;
	};
	
	this.notifyVoltageChanged = function(connectionPoint){
		return;
	};
};
