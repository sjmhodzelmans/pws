function adGate(id){
	this.type = "adGate";
	this.id = id;
	this.cPin;
	this.cPout1;
	this.cPout2;
	this.cPout4;
	this.cPout8;
	
	this.draw = function(x, y, scale){
		if(typeof(scale) === 'undefined') scale = 10;
		var group = new Kinetic.Group({
			x: x,
			y: y,
			draggable: true
		});
		
		var base = new Kinetic.Rect({
	        x: x,
	        y: y,
	        width: 20*scale,
	        height: 10*scale,
	        fill: 'yellow',
	        stroke: 'black',
	        strokeWidth: 2
	    });
		
		var ADtext = new Kinetic.Text({
			x: x+(scale*0),
			y: y+(scale*7),
			width: 20*scale,
			height: 6*scale,
			text: 'AD-omzetter',
			fontFamily: 'Calibri',
	        fontSize: 2*scale,
	        align: 'center',
	        fill: 'black',
	    });
		
		var intext = new Kinetic.Text({
			x: x+(scale*1),
			y: y+(scale*4),
			text: "in",
			fontFamily: 'Calibri',
	        fontSize: 2*scale,
	        width: 2*scale,
			align: 'center',
			fill: 'black',
		});	
		
		var text1 = new Kinetic.Text({
			x: x+(scale*17),
			y: y+(scale*4),
			text: "1",
			fontFamily: 'Calibri',
			fontSize: 2*scale,
			width: 2*scale,
			align: 'center',
			fill: 'black',
		});	
		
		var text2 = new Kinetic.Text({
			x: x+(scale*14),
			y: y+(scale*4),
			text: "2",
			fontFamily: 'Calibri',
			fontSize: 2*scale,
			width: 2*scale,
			align: 'center',
			fill: 'black',
		});	
		
		var text4 = new Kinetic.Text({
			x: x+(scale*11),
			y: y+(scale*4),
			text: "4",
			fontFamily: 'Calibri',
			fontSize: 2*scale,
			width: 2*scale,
			align: 'center',
			fill: 'black',
		});	
		
		var text8 = new Kinetic.Text({
			x: x+(scale*8),
			y: y+(scale*4),
			text: "8",
			fontFamily: 'Calibri',
			fontSize: 2*scale,
			width: 2*scale,
			align: 'center',
			fill: 'black',
		});	
		
		this.cPin = collectionObject.addConnectionPoint(x+(2*scale), y+(2*scale), false, this, true);
	    
		this.cPout1 = collectionObject.addConnectionPoint(x+(18*scale), y+(2*scale), false, this, false);
		this.cPout2 = collectionObject.addConnectionPoint(x+(15*scale), y+(2*scale), false, this, false);
		this.cPout4 = collectionObject.addConnectionPoint(x+(12*scale), y+(2*scale), false, this, false);
		this.cPout8 = collectionObject.addConnectionPoint(x+(9*scale), y+(2*scale), false, this, false);
	    
	    group.add(base);

	    group.add(intext);
	    group.add(ADtext);
	    
	    group.add(text1);
	    group.add(text2);
	    group.add(text4);
	    group.add(text8);
	    
	    group.add(this.cPin.draw(scale));

	    group.add(this.cPout8.draw(scale));
	    group.add(this.cPout4.draw(scale));
	    group.add(this.cPout2.draw(scale));
	    group.add(this.cPout1.draw(scale));
	    
	    var context = this;
	    
	    group.on('click', function(){
	    	if(collectionObject.pointerMode == 'pointer'){
	    		if(collectionObject.pointerMode = 'pointer'){
		    		collectionObject.resetForm();
		    	}
	    	}
	    	if(collectionObject.pointerMode == 'delete'){
	    		collectionObject.deleteGate(context, new Array(context.cPincount, context.cPinon, context.cPinreset, 
	    									context.cPout1, context.cPout2, context.cPout4, context.cPout8), group);
	    	}
	    });
	    
	    group.on('dragend dragmove', function(){
	    	context.cPin.updateWirePositions(); 
			
	    	context.cPout1.updateWirePositions(); 
	    	context.cPout2.updateWirePositions(); 
	    	context.cPout4.updateWirePositions(); 
	    	context.cPout8.updateWirePositions();
	    });
	    return group;
	};
	
	this.notifyVoltageChanged = function(connectionPoint){
		if(connectionPoint == this.cPin){
			var digitalvalue = Math.round((this.cPin.voltage/5)*15);
			console.log("digitalvalue: "+digitalvalue);
			
			var binaryarray = digitalvalue.toString(2).split("").reverse();
			if(binaryarray[0] == "1"){
				this.cPout1.setVoltage(5.0);
				this.cPout1.setState(true);				
			}else{
				this.cPout1.setVoltage(0.0);
				this.cPout1.setState(false);
			}
			if(binaryarray[1] == "1"){
				this.cPout2.setVoltage(5.0);
				this.cPout2.setState(true);				
			}else{
				this.cPout2.setVoltage(0.0);
				this.cPout2.setState(false);
			}
			if(binaryarray[2] == "1"){
				this.cPout4.setVoltage(5.0);
				this.cPout4.setState(true);				
			}else{
				this.cPout4.setVoltage(0.0);
				this.cPout4.setState(false);
			}
			if(binaryarray[3] == "1"){
				this.cPout8.setVoltage(5.0);
				this.cPout8.setState(true);				
			}else{
				this.cPout8.setVoltage(0.0);
				this.cPout8.setState(false);
			}
		}
		return;
	};
};
