<!DOCTYPE HTML>
<html manifest="offline.appcache">
	<head>
		<title>Profielwerkstuk</title>
		<style type="text/css">
			#container, body{
				background-color: #3355FF;
			}
			
			#form{
				margin: 10px;
			}
		</style>
	</head>
	<body>
		<form id="form">
			<label for="value" id="label">Waarde</label>
			
			<input type="number" id="value" max="5.0" min="0.0" step="0.1" />
			<input type="submit" id="submit" value="instellen" />
		</form>
		<audio loop id="buzz">
			<source src="zoem.mp3" type="audio/mpeg"></source>
			<source src="zoem.ogg" type="audio/ogg"></source>
		</audio>	
		
		<div id="container"></div>
		
		<script src="javascript/kinetic.js"></script>
		<script src="javascript/jquery.js"></script>
		<script src="javascript/baseGate.js"></script>
		<script src="javascript/collectionObject.js"></script>
		<script src="javascript/connectionPoint.js"></script>
		<script src="javascript/wire.js"></script>
		<script src="javascript/sourceGate.js"></script>
		<script src="javascript/ledGate.js"></script>
		<script src="javascript/andGate.js"></script>
		<script src="javascript/orGate.js"></script>
		<script src="javascript/notGate.js"></script>
		<script src="javascript/varGate.js"></script>
		<script src="javascript/compGate.js"></script>
		<script src="javascript/memGate.js"></script>
		<script src="javascript/pulsGate.js"></script>
		<script src="javascript/countGate.js"></script>
		<script src="javascript/adGate.js"></script>
		<script src="javascript/buzzGate.js"></script>
		<script>
			$(document).ready(function(){
				var stage = new Kinetic.Stage({
		        	container: 'container',
		        	width: $(window).width() - 10,
					height: $(window).height() - $("#form").height()
			    });
			    $("#form").hide();
	
			    collectionObject.drawBasis(stage);
			});		    
		</script>
	</body>
</html>
